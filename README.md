  
  This parser aim to transform a json string to the desired object.
  
**Disclaimer:** Keep in mind this is a really simple reflection tool which
  use [www.json.org](www.json.org) library to transform
  string json to a object representation and then with a little help of
  annotation and the **JsonParser** map it to your class. So be nice with
  the tool.
  
  Before to use this parser you must annotate your class fields with
**JsonField** to provide the matching field with your json. There is no
  need to have same field name in your class and json string, but must have
  same value as you had defined in your **JsonField** annotation
  
  **For instance:**
  

```
#!java
 public class Profile {
     @JsonField("name")
     private String name;
  
     @JsonField("last-name")
     private String lastName;
     
     ...
     
  }

```

  
```
#!java

String json =  { 
      name : "Bond"
      last-name : "James"
  }
  
```

  Once you had defined your class with annotations you may want to call to the
  parser
  

```
#!java

  Profile profile = JsonParser.fromJson(Profile.class, json);

```
  
  use profile like as a regular class instance
  
  
```
#!java

System.out.println(String.format("My name is %s, %s %s", profile.getLastName(), profile.getName(),
  		profile.getLastName()));
```

  
**Output**
  
  

```
#!java

  My name is Bond, James Bond
```
  
  
 **Supported Mapping**
  

* primitives: int, boolean, double, long
* array of primitives: int[], boolean[], double[], long[]
* Collections: List, ArrayList, Set, etc. **Be aware of type erasure**
* Enum, Array of Enum, Collection of Enum
* Your custom classes
* Nested classes
* Any combination of above
* Anonymous class
  
**Json object as result**
  
  Some times is common return an anonymous object with an attribute to hold the
  whole information, to overcome this particular case JsonParser
  provide a method to indicate how to map that particular field.
  
  **For instance:**
  


```
#!java

   String json =   { 
         profiles : [{ first-name : "Bond", last-name : "James"}]
   }

```
  
  
  Note here we have an anonymous object with an attribute named 'profile', to
  parse this json you must provide the field name to map and the class name to
  apply for it.
  
  
  
```
#!java

  Profile[] profiles = JsonPaser.fromJson("profiles", Profile[].class, json);

```

  
  This is also do-able with **Collections**  
  
```
#!java

List<Profile> profiles = JsonPaser.fromJson("profiles", ArrayList.class, Profile.class, json);

```

  
  for this particular case you must provide the collection's item class type
**Profile.class** to make it works.
  
  
**Important**
  
  So far this parser do not accept default values for your missing fields, in
  that case, an exception will be printed out and a null object will be
  returned.
  
  
**  Nice to have**

  * Default value for missing fields
*   ~~toJson() build a json string representation~~
*   Better handling of error message, instead of print the stack trace

Vidal Santiago Martínez -
vidalsantiagomartinez@gmail.com
  