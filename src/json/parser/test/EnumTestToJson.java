package json.parser.test;

import json.parser.JsonParser;

import org.junit.Assert;
import org.junit.Test;

public class EnumTestToJson {

	private enum Planets {
		MOON, MARS, EARTH
	}

	@Test
	public void enumTest() {
		Planets moon = Planets.MOON;
		String json = JsonParser.toJson(moon); // MOON
		Assert.assertEquals(Planets.MOON, JsonParser.fromJson(Planets.class, json));
	}

	@Test
	public void enumArray() {
		Planets[] expected = new Planets[] { Planets.MOON, Planets.EARTH, Planets.MARS };
		Planets[] actual = JsonParser.fromJson(Planets[].class, JsonParser.toJson(expected));
		Assert.assertArrayEquals(expected, actual);
	}
}
