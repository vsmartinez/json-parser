package json.parser.test;

import json.parser.JsonParser;
import json.parser.test.ProfileCustom.Planets;
import junit.framework.Assert;

import org.junit.Test;

public class CustomObjectTest {

	private String json = " { name = \"Vidal Santiago\", last-name = \"Martinez\", age = 35, single = true, salary = 1000000.50, "
			+ "id = { type = \"dni\", number = 25952958 }, "
			+ "skills = [ \"kick-ass\", \"kung-fu\", \"boxing\", \"sky-diving\" ] , "
			+ "phones = [4651303, 4444444, 4448151], "
			+ "ids = [ { type = \"dni\", number = 25952958, id = { type = \"cuil\", number = 242595295 } }, { type = \"ind\", number = 85925952 }], "
			+ "num = [1,2,3], planet = EARTH, next-planets = [ MOON, MARS ] }";

	@Test
	public void profileTest() {
		ProfileCustom p = JsonParser.fromJson(ProfileCustom.class, json);

		Assert.assertEquals("Vidal Santiago", p.getName());
		Assert.assertEquals("Martinez", p.getLastName());
		Assert.assertEquals(35, p.getAge());
		Assert.assertEquals(true, p.isSingle());
		Assert.assertEquals(1000000.50, p.getSalary());

		Assert.assertEquals("dni", p.getId().getType());
		Assert.assertEquals(25952958, p.getId().getNumber());

		Assert.assertEquals("kick-ass", p.getSkills().get(0));
		Assert.assertEquals("kung-fu", p.getSkills().get(1));
		Assert.assertEquals("boxing", p.getSkills().get(2));
		Assert.assertEquals("sky-diving", p.getSkills().get(3));

		Assert.assertEquals(new Integer(4651303), p.getPhones().get(0));
		Assert.assertEquals(new Integer(4444444), p.getPhones().get(1));
		Assert.assertEquals(new Integer(4448151), p.getPhones().get(2));

		Assert.assertEquals("dni", p.getIds().get(0).getType());
		Assert.assertEquals(25952958, p.getIds().get(0).getNumber());

		Assert.assertEquals("cuil", p.getIds().get(0).getId().getType());
		Assert.assertEquals(242595295, p.getIds().get(0).getId().getNumber());

		Assert.assertEquals("ind", p.getIds().get(1).getType());
		Assert.assertEquals(85925952, p.getIds().get(1).getNumber());

		Assert.assertEquals(1, p.getNum()[0]);
		Assert.assertEquals(2, p.getNum()[1]);
		Assert.assertEquals(3, p.getNum()[2]);

		Assert.assertEquals(Planets.EARTH, p.getPlanet());

		Assert.assertEquals(Planets.MOON, p.getNextPlanets()[0]);
		Assert.assertEquals(Planets.MARS, p.getNextPlanets()[1]);
	}
}
