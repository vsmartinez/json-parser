package json.parser.test;

import json.parser.JsonParser;

import org.junit.Assert;
import org.junit.Test;

public class ArrayTestToJson {

	@Test
	public void string() {
		String[] expected = new String[] { "one", "two", "three" };
		String[] actual = JsonParser.fromJson(String[].class, JsonParser.toJson(expected));
		Assert.assertArrayEquals(expected, actual);
	}

	@Test
	public void intPrimitive() {
		int[] expected = new int[] { 1, 2, 3 };
		int[] actual = JsonParser.fromJson(int[].class, JsonParser.toJson(expected));
		Assert.assertArrayEquals(expected, actual);
	}

	@Test
	public void longPrimitive() {
		long[] expected = new long[] { 1, 2, 3 };
		long[] actual = JsonParser.fromJson(long[].class, JsonParser.toJson(expected));
		Assert.assertArrayEquals(expected, actual);
	}

	@Test
	public void doublePrimitive() {
		double[] expected = new double[] { 1.2, 2.3, 3.4 };
		double[] actual = JsonParser.fromJson(double[].class, JsonParser.toJson(expected));
		Assert.assertArrayEquals(expected, actual, 0.01);
	}

	@Test
	public void booleanPrimitive() {
		boolean[] expected = new boolean[] { true, false, true, true };
		boolean[] actual = JsonParser.fromJson(boolean[].class, JsonParser.toJson(expected));

		Assert.assertTrue(expected.length == actual.length);

		for (int i = 0; i < expected.length; i++) {
			Assert.assertTrue(expected[i] == actual[i]);
		}
	}
}
