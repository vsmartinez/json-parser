package json.parser.test;

import json.parser.JsonParser;

import org.junit.Assert;
import org.junit.Test;

public class PrimitiveBoxingToJsonTest {

	@Test
	public void outboxingInteger() {
		Assert.assertEquals("1", JsonParser.toJson(new Integer(1)));
	}

	@Test
	public void outboxingDouble() {
		Assert.assertEquals("1.0", JsonParser.toJson(new Double(1.0)));
	}

	@Test
	public void outboxingLong() {
		Assert.assertEquals("1", JsonParser.toJson(new Long(1L)));
	}

	@Test
	public void outboxingString() {
		Assert.assertEquals("\"1\"", JsonParser.toJson("1"));
	}

	@Test
	public void outboxingBoolean() {
		Assert.assertEquals("false", JsonParser.toJson(Boolean.FALSE));
		Assert.assertEquals("true", JsonParser.toJson(Boolean.TRUE));
	}

	@Test
	public void primitiveInt() {
		Assert.assertEquals("1", JsonParser.toJson(1));
	}

	@Test
	public void primitiveBoolean() {
		Assert.assertEquals("false", JsonParser.toJson(false));
		Assert.assertEquals("true", JsonParser.toJson(true));
	}

	@Test
	public void primitiveDouble() {
		Assert.assertEquals("1.2", JsonParser.toJson(1.2));
	}

	@Test
	public void primitiveLong() {
		Assert.assertEquals("1", JsonParser.toJson(1L));
	}
}
