package json.parser.test;

import java.util.List;

import json.parser.JsonParser;
import junit.framework.Assert;

import org.junit.Test;

public class CannonicalTest {

	private String jsonProfile = "{\"profiles\": {\"email\":\"jbond@tomorrowneverdie.com\", \"first_name\":\"James\","
			+ "\"last_name\":\"Bond\", \"birth_date\" : \"30/05/1977\"}}";

	@Test
	public void simpleCannonicalTest() {
		String s = JsonParser.fromJson("profiles.email", String.class,
				jsonProfile);
		Assert.assertEquals("jbond@tomorrowneverdie.com", s);
	}

	@Test
	public void deepCannonicalTest() {

		String jsonProfile = "{\"profiles\": {\"email\":\"jbond@tomorrowneverdie.com\", \"first_name\":\"James\","
				+ "\"last_name\":\"Bond\", \"birth_date\" : { \"day\" : \"30\", \"month\" : \"05\", \"year\" : \"1977\"}}}";

		String s = JsonParser.fromJson("profiles.birth_date.day", String.class,
				jsonProfile);
		Assert.assertEquals("30", s);

		s = JsonParser.fromJson("profiles.birth_date.month", String.class,
				jsonProfile);
		Assert.assertEquals("05", s);

		s = JsonParser.fromJson("profiles.birth_date.year", String.class,
				jsonProfile);
		Assert.assertEquals("1977", s);
	}

	@Test
	public void arrayCannonicalTest() {

		String jsonProfile = "{\"profiles\": {\"email\":\"jbond@tomorrowneverdie.com\", \"first_name\":\"James\","
				+ "\"last_name\":\"Bond\", \"birth_date\" : { \"day\" : \"30\", \"month\" : \"05\", \"year\" : \"1977\"},"
				+ " \"guns\" : [ \"45\" , \"luger\" ] }}";

		String[] guns = JsonParser.fromJson("profiles.guns", String[].class,
				jsonProfile);
		Assert.assertEquals("45", guns[0]);
		Assert.assertEquals("luger", guns[1]);
	}

	@Test
	public void arrayElementCannonicalTest() {

		String jsonProfile = "{ \"guns\" : [ \"45\" , \"luger\" ] }}";

		String gun = JsonParser.fromJson("guns[1]", String.class, jsonProfile);
		Assert.assertEquals("luger", gun);
	}

	@Test
	public void arraySubElementCannonicalTest() {

		String jsonProfile = "{\"profiles\": {\"email\":\"jbond@tomorrowneverdie.com\", \"first_name\":\"James\","
				+ "\"last_name\":\"Bond\", \"birth_date\" : { \"day\" : \"30\", \"month\" : \"05\", \"year\" : \"1977\"},"
				+ " \"guns\" : [ { \"name\": \"45\", \"cartridge\" : \"45\"  } , { \"name\" : \"luger\", \"cartridge\" : \"45\"} ] }}";

		String luger = JsonParser.fromJson("profiles.guns[1].cartridge",
				String.class, jsonProfile);
		Assert.assertEquals("45", luger);
	}

	@Test
	public void arrayLastSubElementCannonicalTest() {

		String jsonProfile = "{\"profiles\": {\"email\":\"jbond@tomorrowneverdie.com\", \"first_name\":\"James\","
				+ "\"last_name\":\"Bond\", \"birth_date\" : { \"day\" : \"30\", \"month\" : \"05\", \"year\" : \"1977\"},"
				+ " \"guns\" : [ \"45\", \"luger\"] }}";

		String luger = JsonParser.fromJson("profiles.guns[1]", String.class,
				jsonProfile);
		Assert.assertEquals("luger", luger);
	}

	@Test
	public void arrayLastSubSubElementCannonicalTest() {

		String jsonProfile = "{ \"father\" : { \"sons\" : [ { \"grandsons\" :  [ \"john\", \"michael\", \"sean\" ] } ] } }}";

		String grandson = JsonParser.fromJson("father.sons[0].grandsons[1]",
				String.class, jsonProfile);
		Assert.assertEquals("michael", grandson);
	}

	@SuppressWarnings("static-method")
	@Test
	public void arraySubListCannonicalTest() {

		String jsonProfile = "{ \"father\" : { \"sons\" : [ { \"first_name\" : \"Santiago\" , \"last_name\" : \"Martinez\" }, { \"first_name\" : \"Manuel\" , \"last_name\" : \"Martinez\" } ] } }";

		@SuppressWarnings("unchecked")
		List<Profile> profiles = JsonParser.fromJson("father.sons", List.class,
				Profile.class, jsonProfile);
		Assert.assertNotNull(profiles);
		Assert.assertEquals(profiles.size(), 2);
		Assert.assertEquals(profiles.get(0).getFirstName(), "Santiago");
		Assert.assertEquals(profiles.get(0).getLastName(), "Martinez");
		Assert.assertEquals(profiles.get(1).getFirstName(), "Manuel");
		Assert.assertEquals(profiles.get(1).getLastName(), "Martinez");
	}

}
