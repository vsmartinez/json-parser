package json.parser.test;

import json.parser.JsonParser;

public class MapToJson {

	public static void main(String[] args) {

		System.out.println(new JsonParser().add("uno", "1").add("dos", "2").add("tres", 3)
				.add("cuatro", new Object[] { "2", "2.1", "2.2" }).toJson());

		System.out.println(new JsonParser("numbers").add("one", "1").add("two", "2").add("three", 3)
				.add("four", new Object[] { "4", "4.1", "4.2" }).toJson());

		System.out.println(new JsonParser("numeros").add("uno", "1").add("dos", "2").add("3", "tres").add("tres", 3)
				.add("cuatro", new Object[] { "2", "2.1", "2.2" }).add("1", "fancy")
				.add("cuatrodecuatro", new Object[] { "2", new String[] { "2.2", "2.2.1", "2.2.2" }, 2.2 }).toJson());

		System.out.println(new JsonParser("numeros").add("uno", "1").add("dos", "2").add("tres", 3).object("hijos")
				.add("hijok1", "hijov1").add("hijok2", "hijov2").end().toJson());

		System.out.println(new JsonParser().add("uno", "1").add("dos", "2").add("tres", 3).object("hijos")
				.add("hijok1", "hijov1").

				object("nietos").add("nietok1", "nietov1").add("nietok2", "nietov2").end()

				.add("hijok2", "hijov2").end().add("cuatro", "4").toJson());

		System.out.println("------------");
		System.out.println(new JsonParser("numeros").add("uno", "1").add("dos", "2").add("tres", 3)
				.add("cuatro", "2", "2.1", "2.2").toJson());

		System.out.println(new JsonParser("numeros").add("uno", "1").add("dos", "2").add("tres", 3)
				.add("cuatro", "vidal", "santiago", "martinez").toJson());

		System.out.println(new JsonParser("numeros").add("uno", "1").add("dos", "2").add("tres", 3)
				.add("cuatro", "vidal", 2, "martinez").toJson());

		System.out.println(new JsonParser("numeros").add("uno", "1").add("dos", "2").add("tres", 3)
				.add("4", "vidal", 2, "martinez").add("5", true).toJson());

		System.out.println(new JsonParser().array("errors").item()
				.add("errorMessage", "Invalid email address 'test@mpo.com'.").add("errorCode", "1047").end().item()
				.add("otro", "mas").end().array("errors2").item()
				.add("errorMessage", "Invalid email address 'test@mpo.com'.").add("errorCode", "1047").end().item()
				.add("otro", "mas").end().end().end().toJson());

		System.out.println(new JsonParser().object("errors").end().toJson());

		System.out.println(new JsonParser().array("errors").item()
				.add("errorMessage", "Invalid email address 'test@mpo.com'.").add("errorCode", "1047").add("sinnada")
				.end().item().array("roles").item().add("role", "admin").add("role2", "user").end().end().end().end()
				.add("afuera", "de todo").toJson());

		System.out.println(new JsonParser().array("errors").item()
				.add("errorMessage", "Invalid email address 'test@mpo.com'.").add("errorCode", "1047").add("sinnada")
				.end().item().add("errorM", "Invalid email address 'test@mpo.com'.").add("errorC", "1047").end().end()
				.object("adentro").add("a1", "a1v").add("a1", "a1v").end().array("roles").item().add("role", "admin")
				.add("role2", "user").end().end().add("afuera", "de todo").toJson());

		System.out.println(new JsonParser("afeura").add("mismonivel", "vaor").object("adentro").add("a1", "a1v")
				.add("a2", "a1v").object("aadentro").add("a1", "a1v").end().end().object("2adentro").add("a1", "a1v")
				.end().array("arreglo").item().add("clave", "value").add("clave1", "value").end().item()
				.add("clave20", "value").add("clave21", "value").end().end().object("objecto-arreglo2")
				.array("arreglo-este").item().add("item", "value").add("item2", "value").end().end()
				.array("arreglo2-otro").item().add("item3", "value").add("item4", "value").end().end().end().toJson());

		System.out.println(new JsonParser("afeura").add("mismonivel", "vaor").array("arreglo-este").item()
				.add("item", "value").add("item2", "value").end().end().array("arreglo2-otro").item()
				.array("arreglo22-otro").item().add("item3", "value").add("item4", "value").end().end()
				.add("item3", "value").add("item4", "value").end().end().toJson());

		// {"providers"  : ["name":"DIGITALSMITHS", {"types":[{"type":"MOVIE"}]}],
		//  "searchQuery": {"terms":[{ "value": "Comedy","operator":"AND","field": "GENRES"}]},
		//  "count"      : 36,
//	         "startIndex": 0}
		
//		   {"providers"  : [{ "name" : "DIGITALSMITHS", "types" : [{ "type" : "MOVIE" }]}], 
//		    "searchQuery": { "terms" : [{"field" : "GENRES", "value" : "Comedy", "operator" : "AND" }]}
//		    "startIndex" : 0, 
//		         "count" : 36}


		// System.out.println(new
		// JsonParser().array("providers").array("types").end().end().object("searchQuery")
		// .array("terms").end().end().add("count", 36).add("count",
		// 36).add("statrIndex", 0).toJson());
		//
		System.out.println(new JsonParser().array("providers").item().array("types").item().add("type", "MOVIE").end()
				.add("name", "DIGITALSMITHS").end().end().object("searchQuery").array("terms").item()
				.add("value", "Comedy").add("operator", "AND").add("field", "GENRES").end().end().end().end().add("count", 36).add("startIndex", 0).toJson());

		System.out.println(new JsonParser().array("aca").item().add("type", "MOVIE").end().add("adsfas", "adfads").toJson());
	}
}
