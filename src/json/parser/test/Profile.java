package json.parser.test;

import json.parser.JsonField;
import json.parser.JsonObject;

@JsonObject("profile")
public class Profile {

	@JsonField("email")
	private String email;

	@JsonField("partner_profile_id")
	private String partnerProfileId;

	@JsonField("first_name")
	private String firstName;

	@JsonField("last_name")
	private String lastName;

	@JsonField("address_street")
	private String address;

	@JsonField("address_street_2")
	private String secondaryAddress;

	@JsonField("address_city")
	private String city;

	@JsonField("address_state")
	private String state;

	@JsonField("address_country")
	private String country;

	@JsonField("address_postal_code")
	private String postalCode;

	@JsonField("phone")
	private String phone;

	@JsonField("monthly_spending_limit")
	private String monthlyLimit;

	@JsonField("parental_control_tv")
	private String parentalTv;

	@JsonField("parental_control_movie")
	private String parentalMovie;

	@JsonField("avatar_url")
	private String avatar;

	@JsonField("eula_accepted")
	private String eulaAccepted;

	@JsonField("eula_version")
	private String eulaVersion;

	@JsonField("user_name")
	private String userName;

	@JsonField("user_nick_name")
	private String nickName;

	@JsonField("epg_headend")
	private String epg;

	@JsonField("profile_id")
	private String profileId;

	@JsonField("carrier")
	private String carrier;

	@JsonField("product")
	private String product;

	@JsonField("account_id")
	private String accountId;

	@JsonField("is_admin_profile")
	private String isAdmin;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPartnerProfileId() {
		return partnerProfileId;
	}

	public void setPartnerProfileId(String partnerProfileId) {
		this.partnerProfileId = partnerProfileId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSecondaryAddress() {
		return secondaryAddress;
	}

	public void setSecondaryAddress(String secondaryAddress) {
		this.secondaryAddress = secondaryAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMonthlyLimit() {
		return monthlyLimit;
	}

	public void setMonthlyLimit(String monthlyLimit) {
		this.monthlyLimit = monthlyLimit;
	}

	public String getParentalTv() {
		return parentalTv;
	}

	public void setParentalTv(String parentalTv) {
		this.parentalTv = parentalTv;
	}

	public String getParentalMovie() {
		return parentalMovie;
	}

	public void setParentalMovie(String parentalMovie) {
		this.parentalMovie = parentalMovie;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getEulaAccepted() {
		return eulaAccepted;
	}

	public void setEulaAccepted(String eulaAccepted) {
		this.eulaAccepted = eulaAccepted;
	}

	public String getEulaVersion() {
		return eulaVersion;
	}

	public void setEulaVersion(String eulaVersion) {
		this.eulaVersion = eulaVersion;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEpg() {
		return epg;
	}

	public void setEpg(String epg) {
		this.epg = epg;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}

}
