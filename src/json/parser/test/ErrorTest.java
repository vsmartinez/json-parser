package json.parser.test;

import json.parser.JsonParser;

import org.junit.Assert;
import org.junit.Test;

public class ErrorTest {

	@Test
	public void quotedObjectTest() {

		Error[] expecteds = new Error[] { new Error("Invalid email address 'test@dummy.com'.", "1047") };
		Error[] actuals = JsonParser.fromJson("errors", Error[].class, JsonParser.toJson("errors", expecteds));

		Assert.assertTrue(expecteds[0].getMessage().equals(actuals[0].getMessage()));
		Assert.assertTrue(expecteds[0].getCode().equals(actuals[0].getCode()));
	}
}
