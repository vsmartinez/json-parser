package json.parser.test;

import json.parser.JsonParser;

import org.junit.Assert;
import org.junit.Test;

public class ArrayTest {

	@Test
	public void string() {
		String json = "[ \"one\", \"two\", \"three\" ]";
		String[] expected = new String[] { "one", "two", "three" };
		Assert.assertArrayEquals(expected, JsonParser.fromJson(String[].class, json));
	}

	@Test
	public void intPrimitive() {
		String json = "[ 1, 2, 3 ]";
		int[] expected = new int[] { 1, 2, 3 };
		Assert.assertArrayEquals(expected, JsonParser.fromJson(int[].class, json));
	}

	@Test
	public void longPrimitive() {
		String json = "[ 1, 2, 3 ]";
		long[] expected = new long[] { 1, 2, 3 };
		Assert.assertArrayEquals(expected, JsonParser.fromJson(long[].class, json));
	}

	@Test
	public void doublePrimitive() {
		String json = "[ 1.2, 2.3, 3.4 ]";
		double[] expected = new double[] { 1.2, 2.3, 3.4 };
		Assert.assertArrayEquals(expected, JsonParser.fromJson(double[].class, json), 0.01);
	}

	@Test
	public void booleanPrimitive() {
		String json = "[ true, false, true, true ]";
		boolean[] expected = new boolean[] { true, false, true, true };
		boolean[] actual = JsonParser.fromJson(boolean[].class, json);

		Assert.assertTrue(expected.length == actual.length);

		for (int i = 0; i < expected.length; i++) {
			Assert.assertTrue(expected[i] == actual[i]);
		}
	}
}
