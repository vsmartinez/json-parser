package json.parser.test;

import json.parser.JsonParser;

import org.junit.Assert;
import org.junit.Test;

public class EnumTest {

	private enum Planets {
		MOON, MARS, EARTH
	}

	@Test
	public void enumTest() {
		String json = "MOON";
		Assert.assertEquals(Planets.MOON, JsonParser.fromJson(Planets.class, json));
	}

	@Test
	public void enumArray() {
		String json = "[ MOON, EARTH, MARS ]";
		Planets[] expected = new Planets[] { Planets.MOON, Planets.EARTH, Planets.MARS };
		Assert.assertArrayEquals(expected, JsonParser.fromJson(Planets[].class, json));
	}
}
