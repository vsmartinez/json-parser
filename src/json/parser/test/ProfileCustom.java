package json.parser.test;

import java.util.List;

import json.parser.JsonField;
import json.parser.JsonObject;

@JsonObject("profile")
public class ProfileCustom {

	@JsonField("name")
	private String name;

	@JsonField("last-name")
	private String lastName;

	@JsonField(value = "age")
	private int age;

	@JsonField("single")
	private boolean single;

	@JsonField("salary")
	private double salary;

	@JsonField("id")
	private Id id;

	@JsonObject("id")
	public class Id {
		@JsonField("type")
		private String type;

		@JsonField("number")
		private int number;
		
		@JsonField("id")
		private Id id;

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public int getNumber() {
			return number;
		}

		public void setNumber(int number) {
			this.number = number;
		}

		public Id getId() {
			return id;
		}

		public void setId(Id id) {
			this.id = id;
		}
	}

	@JsonField("skills")
	private List<String> skills;

	@JsonField("phones")
	private List<Integer> phones;

	@JsonField("ids")
	private List<Id> ids;

	@JsonField("num")
	private int[] num;
	
	@JsonField("alias")
	private String[] alias;
	
	
	public void setName(String name) {
		this.name = name;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public int getAge() {
		return age;
	}

	public boolean isSingle() {
		return single;
	}

	public void setSingle(boolean single) {
		this.single = single;
	}

	public double isSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Id getId() {
		return id;
	}

	public void setId(Id id) {
		this.id = id;
	}

	public double getSalary() {
		return salary;
	}

	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}

	public List<Integer> getPhones() {
		return phones;
	}

	public void setPhones(List<Integer> phones) {
		this.phones = phones;
	}

	public List<Id> getIds() {
		return ids;
	}

	public void setIds(List<Id> ids) {
		this.ids = ids;
	}

	public int[] getNum() {
		return num;
	}

	public void setNum(int[] num) {
		this.num = num;
	}
	
	public enum Planets {
		MOON, MARS, EARTH
	}

	@JsonField("planet")
	private Planets planet;
	
	@JsonField("next-planets")
	private Planets[] nextPlanets;

	public Planets getPlanet() {
		return planet;
	}

	public void setPlanet(Planets myPlace) {
		this.planet = myPlace;
	}

	public Planets[] getNextPlanets() {
		return nextPlanets;
	}

	public void setNextPlanets(Planets[] nextPlanets) {
		this.nextPlanets = nextPlanets;
	}

	public String[] getAlias() {
		return alias;
	}

	public void setAlias(String[] alias) {
		this.alias = alias;
	}

}
