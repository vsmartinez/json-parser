package json.parser.test;

import json.parser.JsonField;
import json.parser.JsonObject;

@JsonObject("error")
public class Error {

	@JsonField("errorMessage")
	private String message;

	@JsonField("errorCode")
	private String code;

	public Error(){
		
	}
	
	public Error(String message, String code) {
		this.message = message;
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
