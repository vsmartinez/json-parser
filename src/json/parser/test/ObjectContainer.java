package json.parser.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import json.parser.JsonField;
import json.parser.JsonParser;
import json.parser.JsonParser.JsonTransformer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ObjectContainer {

	public class Profile {

		@JsonField("email")
		String email;

		@JsonField("first_name")
		String firstName;

		@JsonField("last_name")
		String lastName;

		@JsonField("birth_date")
		Date birthDate;

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public Date getBirthDate() {
			return birthDate;
		}

		public void setBirthDate(Date birthDate) {
			this.birthDate = birthDate;
		}
	}

	private String jsonProfiles = "{\"profiles\":[ {\"email\":\"jbond@tomorrowneverdie.com\", \"first_name\":\"James\","
			+ "\"last_name\":\"Bond\"}]}";

	@Before
	public void setup() {
		JsonParser.addTransformer(Date.class, new JsonTransformer<Date>() {

			@Override
			public Date reader(String value) {
				if (value == null) {
					return null;
				}

				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				try {
					return dateFormat.parse(value);
				} catch (ParseException e) {
					return null;
				}
			}

			@Override
			public String writer(Date value) {
				// TODO Auto-generated method stub
				return null;
			}

		});
	}

	@Test
	public void objectContainerWithArray() {
		Profile[] p = JsonParser.fromJson("profiles", Profile[].class, jsonProfiles);
		Assert.assertEquals(p[0].getEmail(), "jbond@tomorrowneverdie.com");
		Assert.assertEquals(p[0].getFirstName(), "James");
		Assert.assertEquals(p[0].getLastName(), "Bond");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void objectContainerWithCollection() {
		List<Profile> p = JsonParser.fromJson("profiles", ArrayList.class, Profile.class, jsonProfiles);
		Assert.assertEquals(p.get(0).getEmail(), "jbond@tomorrowneverdie.com");
		Assert.assertEquals(p.get(0).getFirstName(), "James");
		Assert.assertEquals(p.get(0).getLastName(), "Bond");
	}

	private String jsonProfile = "{\"profiles\": {\"email\":\"jbond@tomorrowneverdie.com\", \"first_name\":\"James\","
			+ "\"last_name\":\"Bond\", \"birth_date\" : \"30/05/1977\"}}";

	@Test
	public void objectContainer() {
		Profile p = JsonParser.fromJson("profiles", Profile.class, jsonProfile);
		Assert.assertEquals(p.getEmail(), "jbond@tomorrowneverdie.com");
		Assert.assertEquals(p.getFirstName(), "James");
		Assert.assertEquals(p.getLastName(), "Bond");
		try {
			Assert.assertEquals(p.getBirthDate(), new SimpleDateFormat("dd/MM/yyyy").parse("30/05/1977"));
		} catch (ParseException e) {
			Assert.fail();
		}
	}
}
