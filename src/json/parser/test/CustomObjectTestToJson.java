package json.parser.test;

import java.util.ArrayList;

import org.junit.Test;

import json.parser.JsonParser;
import json.parser.test.ProfileCustom.Id;
import json.parser.test.ProfileCustom.Planets;
import junit.framework.Assert;

public class CustomObjectTestToJson {

	@Test
	public void customObjectToJson() {
		ProfileCustom expected = new ProfileCustom();
		expected.setAge(35);
		expected.setName("Santiago");
		expected.setSingle(true);
		expected.setNum(new int[] { 1, 2, 3 });
		expected.setAlias(new String[] { "Vidal", "Santiago", "Martinez" });
		expected.setNextPlanets(new Planets[] { Planets.EARTH, Planets.MARS });
		expected.setSkills(new ArrayList<String>() {
			private static final long serialVersionUID = 8408568569404973835L;

			{
				add("vidal");
				add("santiago");
				add("martinez");
			}
		});

		Id expectedId = expected.new Id();
		expectedId.setNumber(1920);
		expectedId.setType("numero");

		expected.setId(expectedId);

		Id actualId = JsonParser.fromJson(Id.class, JsonParser.toJson(expectedId));

		Assert.assertEquals(expectedId.getNumber(), actualId.getNumber());
		Assert.assertEquals(expectedId.getType(), actualId.getType());
		Assert.assertEquals(expectedId.getId(), actualId.getId());

		ProfileCustom actual = JsonParser.fromJson(ProfileCustom.class, JsonParser.toJson(expected));

		Assert.assertEquals(expected.getAge(), actual.getAge());
		Assert.assertEquals(expected.getLastName(), actual.getLastName());
		Assert.assertEquals(expected.getName(), actual.getName());
		Assert.assertEquals(expected.getSalary(), actual.getSalary());

		Assert.assertTrue(expected.getAlias().length == actual.getAlias().length);
		for (int i = 0; i < expected.getAlias().length; i++) {
			Assert.assertEquals(expected.getAlias()[i], actual.getAlias()[i]);
		}

		Assert.assertEquals(expected.getId().getNumber(), actual.getId().getNumber());
		Assert.assertEquals(expected.getId().getType(), actual.getId().getType());
		Assert.assertEquals(expected.getId().getId(), actual.getId().getId());

		Assert.assertEquals(expected.getIds(), actual.getIds());

		Assert.assertTrue(expected.getNextPlanets().length == actual.getNextPlanets().length);
		for (int i = 0; i < expected.getNextPlanets().length; i++) {
			Assert.assertEquals(expected.getNextPlanets()[i], actual.getNextPlanets()[i]);
		}

		Assert.assertTrue(expected.getNum().length == actual.getNum().length);
		for (int i = 0; i < expected.getNum().length; i++) {
			Assert.assertEquals(expected.getNum()[i], actual.getNum()[i]);
		}

		Assert.assertEquals(expected.getPhones(), actual.getPhones());

		Assert.assertEquals(expected.getPlanet(), actual.getPlanet());

		Assert.assertTrue(expected.getSkills().size() == actual.getSkills().size());
		for (int i = 0; i < expected.getSkills().size(); i++) {
			Assert.assertEquals(expected.getSkills().get(i), actual.getSkills().get(i));
		}
	}
}
