package json.parser.test;

import java.util.ArrayList;
import java.util.HashMap;

import json.parser.JsonParser;
import junit.framework.Assert;

import org.junit.Test;

public class ArrayBuilderTest {

	@Test
	public void arrayFromMap() {
		String json = new JsonParser().add("errors", (Object) new HashMap[] { new HashMap<String, String>() {
			private static final long serialVersionUID = 1541113302267592140L;

			{
				put("errorMessage", "Invalid email address");
				put("errorCode", "405");
			}
		} }).toJson();

		Assert.assertEquals(
				"{ \"errors\" : [ { \"errorMessage\" : \"Invalid email address\", \"errorCode\" : \"405\" } ] }", json);
	}

	@Test
	public void arrayFromArrayList() {
		String str = new JsonParser().add("uno", new ArrayList<String>() {
			private static final long serialVersionUID = 6594133755705677862L;

			{
				add("uno");
				add("dos");
				add("tres");
			}
		}).toJson();

		Assert.assertEquals("{ \"uno\" : [ \"uno\", \"dos\", \"tres\" ] }", str);
	}
}
