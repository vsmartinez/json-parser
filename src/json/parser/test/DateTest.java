package json.parser.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import json.parser.JsonParser;
import json.parser.JsonParser.JsonTransformer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DateTest {

	@Before
	public void setup() {
		JsonParser.addTransformer(Date.class, new JsonTransformer<Date>() {

			@Override
			public Date reader(String value) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				try {
					return dateFormat.parse(value);
				} catch (ParseException e) {
					return null;
				}
			}

			@Override
			public String writer(Date value) {
				return value.toString();
			}

		});
	}

	@Test
	public void simpleDateTest() {
		Date d = JsonParser.fromJson(Date.class, "30/05/1977");

		try {
			Assert.assertEquals(d, new SimpleDateFormat("dd/MM/yyyy").parse("30/05/1977"));
		} catch (ParseException e) {
			Assert.fail();
		}
	}

	@Test
	public void fieldDateTest() {
		Date d = JsonParser.fromJson("date", Date.class, "{ \"date\" = \"31/05/1977\"}");

		try {
			Assert.assertEquals(d, new SimpleDateFormat("dd/MM/yyyy").parse("31/05/1977"));
		} catch (ParseException e) {
			Assert.fail();
		}
	}
}
