package json.parser.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import json.parser.JsonParser;
import json.parser.JsonParser.JsonTransformer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DateTestToJson {

	@Before
	public void setup() {
		JsonParser.addTransformer(Date.class, new JsonTransformer<Date>() {

			private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

			@Override
			public Date reader(String value) {
				try {
					return dateFormat.parse(value);
				} catch (ParseException e) {
					return null;
				}
			}

			@Override
			public String writer(Date value) {
				return dateFormat.format(value);
			}

		});
	}

	@Test
	public void simpleDateToJsonTest() {
		try {
			Assert.assertEquals("\"30/05/1977\"", JsonParser.toJson(new SimpleDateFormat("dd/MM/yyyy").parse("30/05/1977")));
		} catch (ParseException e1) {
			Assert.fail();
		}
	}

	@Test
	public void fieldDateToJsonTest() {
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse("30/05/1977");
			String str = JsonParser.toJson("date", date);
			Assert.assertEquals(date, JsonParser.fromJson("date", Date.class, str));
		} catch (ParseException e) {
			Assert.fail();
		}
	}
}
