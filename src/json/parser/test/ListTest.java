package json.parser.test;

import java.util.ArrayList;
import java.util.List;

import json.parser.JsonParser;

import org.junit.Assert;
import org.junit.Test;

public class ListTest {

	@Test
	public void listString() {
		String json = "[ \"one\", \"two\", \"three\" ]";
		List<String> expected = new ArrayList<String>() {
			private static final long serialVersionUID = -7463120191201398164L;

			{
				add("one");
				add("two");
				add("three");
			}
		};

		/*
		 * Be aware of Type erasure here
		 */
		@SuppressWarnings("unchecked")
		List<String> actual = JsonParser.fromJson(ArrayList.class, json);

		Assert.assertArrayEquals(expected.toArray(new String[expected.size()]),
				actual.toArray(new String[actual.size()]));
	}

	@Test
	public void listInt() {
		String json = "[ 1, 2, 3 ]";
		List<Integer> expected = new ArrayList<Integer>() {
			private static final long serialVersionUID = -7463120191201398164L;

			{
				add(1);
				add(2);
				add(3);
			}
		};

		/*
		 * Be aware of Type erasure here
		 */
		@SuppressWarnings("unchecked")
		List<String> actual = JsonParser.fromJson(ArrayList.class, json);

		Assert.assertArrayEquals(expected.toArray(new Integer[expected.size()]),
				actual.toArray(new Integer[actual.size()]));
	}

	@Test
	public void listLong() {
		String json = String.format("[ %s, %s, %s ]", Long.MAX_VALUE, Long.MAX_VALUE, Long.MAX_VALUE);
		List<Long> expected = new ArrayList<Long>() {
			private static final long serialVersionUID = -7463120191201398164L;

			{
				add(Long.MAX_VALUE);
				add(Long.MAX_VALUE);
				add(Long.MAX_VALUE);
			}
		};

		/*
		 * Be aware of Type erasure here
		 */
		@SuppressWarnings("unchecked")
		List<Long> actual = JsonParser.fromJson(ArrayList.class, json);

		Assert.assertTrue(expected.size() == actual.size());

		for (int i = 0; i < expected.size(); i++) {
			Assert.assertEquals(expected.get(i), actual.get(i));
		}
	}

	@Test
	public void listDouble() {
		String json = "[ 1.1, 1.2, 1.3 ]";
		List<Double> expected = new ArrayList<Double>() {
			private static final long serialVersionUID = -7463120191201398164L;

			{
				add(1.1);
				add(1.2);
				add(1.3);
			}
		};

		/*
		 * Be aware of Type erasure here
		 */
		@SuppressWarnings("unchecked")
		List<Long> actual = JsonParser.fromJson(ArrayList.class, json);

		Assert.assertTrue(expected.size() == actual.size());

		for (int i = 0; i < expected.size(); i++) {
			Assert.assertEquals(expected.get(i), actual.get(i));
		}
	}

	@Test
	public void listBoolean() {
		String json = "[ true, false, false ]";
		List<Boolean> expected = new ArrayList<Boolean>() {
			private static final long serialVersionUID = -7463120191201398164L;

			{
				add(true);
				add(false);
				add(false);
			}
		};

		/*
		 * Be aware of Type erasure here
		 */
		@SuppressWarnings("unchecked")
		List<Long> actual = JsonParser.fromJson(ArrayList.class, json);

		Assert.assertTrue(expected.size() == actual.size());

		for (int i = 0; i < expected.size(); i++) {
			Assert.assertEquals(expected.get(i), actual.get(i));
		}

	}

	private enum Planets {
		MOON, MARS, EARTH
	}

	@Test
	public void listEnum() {
		String json = "[ MOON, MARS, EARTH ]";
		List<Planets> expected = new ArrayList<Planets>() {
			private static final long serialVersionUID = -7463120191201398164L;

			{
				add(Planets.MOON);
				add(Planets.MARS);
				add(Planets.EARTH);
			}
		};

		/*
		 * Be aware of Type erasure here
		 */
		@SuppressWarnings("unchecked")
		List<Planets> actual = JsonParser.fromJson(ArrayList.class, json);

		actual.get(0);

		Assert.assertTrue(expected.size() == actual.size());

		for (int i = 0; i < expected.size(); i++) {
			/*
			 * Type erasure here
			 */
			Assert.assertEquals(expected.get(i).toString(), actual.get(i));
		}

	}
}
