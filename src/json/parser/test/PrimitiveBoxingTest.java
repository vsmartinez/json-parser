package json.parser.test;

import json.parser.JsonParser;

import org.junit.Assert;
import org.junit.Test;

public class PrimitiveBoxingTest {

	@Test
	public void outboxingInteger() {
		Assert.assertEquals(new Integer(1), JsonParser.fromJson(Integer.class, "1"));
	}

	@Test
	public void outboxingDouble() {
		Assert.assertEquals(new Double(1.0), JsonParser.fromJson(Double.class, "1"));
	}

	@Test
	public void outboxingLong() {
		Assert.assertEquals(new Long(1L), JsonParser.fromJson(Long.class, "1"));
	}

	@Test
	public void outboxingString() {
		Assert.assertEquals("1", JsonParser.fromJson(String.class, "1"));
	}

	@Test
	public void outboxingBoolean() {
		Assert.assertEquals(false, JsonParser.fromJson(Boolean.class, "false"));
		Assert.assertEquals(true, JsonParser.fromJson(Boolean.class, "true"));
	}

	@Test
	public void primitiveInt() {
		Assert.assertTrue(1 == JsonParser.fromJson(int.class, "1"));
	}

	@Test
	public void primitiveBoolean() {
		Assert.assertEquals(true, JsonParser.fromJson(boolean.class, "true"));
		Assert.assertEquals(false, JsonParser.fromJson(boolean.class, "false"));
	}

	@Test
	public void primitiveDouble() {
		Assert.assertEquals(1.2, JsonParser.fromJson(double.class, "1.2"), 0.01);
	}

	@Test
	public void primitiveLong() {
		Assert.assertTrue(1L == JsonParser.fromJson(long.class, "1"));
	}
}
