package json.parser;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * <p>
 * This parser aim to transform a json string to the desired object.
 * </p>
 * <p>
 * <b>Disclaimer:</b> Keep in mind this is a really simple reflection tool which
 * use <a href="http://www.json.org">www.json.org</a> library to transform
 * string json to a object representation and then with a little help of
 * annotation and the {@link JsonParser} map it to your class. So be nice with
 * the tool.
 * </p>
 * <p>
 * Before to use this parser you must annotate your class fields with
 * {@link JsonField} to provide the matching field with your json. There is no
 * need to have same field name in your class and json string, but must have
 * same value as you had defined in your {@link JsonField} annotation
 * <p>
 * <b> For instance:</b>
 * </p>
 * 
 * <pre>
 * public class Profile {
 *    @JsonField("name")
 *    private String name;
 * 
 *    @JsonField("last-name")
 *    private String lastName;
 *    
 *    ...
 *    
 * }
 * </pre>
 * 
 * <pre>
 * String json =  { 
 *     name : "Bond"
 *     last-name : "James"
 * }
 * </pre>
 * 
 * Once you had defined your class with annotations you may want to call to the
 * parser
 * 
 * <pre>
 * Profile profile = JsonParser.fromJson(Profile.class, json);
 * </pre>
 * 
 * <p>
 * use profile like as a regular class instance
 * </p>
 * 
 * <pre>
 * System.out.println(String.format(&quot;My name is %s, %s %s&quot;, profile.getLastName(), profile.getName(),
 * 		profile.getLastName()));
 * </pre>
 * 
 * <p>
 * <b>Output</b>
 * <p>
 * 
 * <pre>
 * My name is Bond, James Bond
 * </pre>
 * 
 * <p>
 * <b>Supported Mapping</b>
 * </p>
 * <p>
 * <ul>
 * <li>primitives: int, boolean, double, long
 * <li>array of primitives: int[], boolean[], double[], long[]
 * <li>Collections: List, ArrayList, Set, etc. <b>Be aware of type erasure</b>
 * <li>Enum, Array of Enum, Collection of Enum
 * <li>Your custom classes
 * <li>Nested classes
 * <li>Any combination of above
 * <li>Anonymous class
 * </ul>
 * </p>
 * <p>
 * <b>Json object as result</b>
 * </p>
 * <p>
 * Some times is common return an anonymous object with an attribute to hold the
 * whole information, to overcome this particular case {@link JsonParser}
 * provide a method to indicate how to map that particular field.
 * </p>
 * <p>
 * <b>For instance:</b>
 * </p>
 * 
 * <pre>
 *  String json =   { 
 *        profiles : [{ first-name : "Bond", last-name : "James"}]
 *  }
 * </pre>
 * 
 * <p>
 * Note here we have an anonymous object with an attribute named 'profile', to
 * parse this json you must provide the field name to map and the class name to
 * apply for it.
 * <p>
 * 
 * <pre>
 * Profile[] profiles = JsonPaser.fromJson(&quot;profiles&quot;, Profile[].class, json);
 * </pre>
 * 
 * <p>
 * This is also do-able with <em>Collections</em>
 * </p>
 * 
 * <pre>
 * List&lt;Profile&gt; profiles = JsonPaser.fromJson(&quot;profiles&quot;, ArrayList.class, Profile.class, json);
 * </pre>
 * 
 * <p>
 * for this particular case you must provide the collection's item class type
 * <code>Profile.class</code> to make it works.
 * </p>
 * 
 * <p>
 * <b>Important</b>
 * </p>
 * <p>
 * So far this parser do not accept default values for your missing fields, in
 * that case, an exception will be printed out and a null object will be
 * returned.
 * </p>
 * 
 * <p>
 * <b>Nice to have</b>
 * <ul>
 * <li>Default value for missing fields
 * <li>toJson() build a json string representation
 * <li>Better handling of error message, instead of print the stack trace
 * </ul>
 * </p>
 * 
 * 
 * @author <a href="mailto:vidalsantiagomartinez@gmail.com">Vidal Santiago
 *         Martínez</a>
 * 
 */
public class JsonParser {

	private String field = null;

	private Deque<String> arrayKey = new ArrayDeque<String>();

	private Deque<Boolean> isArray = new ArrayDeque<Boolean>();

	private Deque<Map<Object, Object>> maps = new ArrayDeque<Map<Object, Object>>();

	public JsonParser() {
		this(null);
	}

	public JsonParser(String field) {
		this.field = field;
		this.maps.push(new HashMap<Object, Object>());
	}

	public JsonParser add(String key, Object value) {
		maps.peek().put(key, value);
		return this;
	}

	public JsonParser add(String key, Object... value) {
		if (value == null || value.length == 0) {
			return this;
		}

		maps.peek().put(key, value);
		return this;
	}

	public JsonParser object(String key) {
		Map<Object, Object> child = new HashMap<Object, Object>();
		this.maps.peek().put(key, child);
		this.maps.push(child);
		this.isArray.push(false);
		return this;
	}

	public JsonParser array(String key) {
		this.maps.peek().put(key, new ArrayList<Map<Object, Object>>());
		this.arrayKey.push(key);
		this.isArray.push(true);
		return this;
	}

	@SuppressWarnings("unchecked")
	public JsonParser item() {
		Map<Object, Object> child = new HashMap<Object, Object>();
		Map<Object, Object> c = this.maps.peek();
		((List<Object>) c.get(arrayKey.peek())).add(child);
		this.maps.push(child);
		isArray.push(false);
		return this;
	}

	public JsonParser end() {
		if (isArray.pop()) {
			arrayKey.pop();
		} else {
			this.maps.pop();
		}

		return this;
	}

	public String toJson() {
		StringWriter writer = new StringWriter();
		if (field != null) {
			writer.write("{ ");
			writeQuoted(field, writer);
			writer.write(" : ");
		}

		writeMap(maps.peekLast(), writer);

		if (field != null) {
			writer.write(" }");
		}

		return writer.toString();
	}

	private static void writeArray(Object o, Writer writer) {

		try {

			if (o == null) {
				writer.append("null");
				return;
			}

			writer.append("[ ");

			int size = Array.getLength(o);
			for (int i = 0; i < size; i++) {
				toJson(Array.get(o, i), writer);
				if (i != size - 1) {
					writer.append(", ");
				}
			}

			writer.append(" ]");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void writeMap(Map<?, ?> map, Writer writer) {
		try {
			writer.append("{ ");

			Object[] keys = map.keySet().toArray();

			for (Object key : keys) {

				toJson(key, writer);

				writer.append(" : ");

				toJson(map.get(key), writer);

				if (key != keys[keys.length - 1]) {
					writer.append(", ");
				}
			}

			writer.append(" }");

		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private static void writeQuoted(String str, Writer writer) {
		try {
			writer.append("\"");
			writer.append(str);
			writer.append("\"");
		} catch (IOException e) {
			// nothing to do
		}
	}

	private static void writeObject(String field, String value, Writer writer) {
		try {
			writer.append("{");
			writeQuoted(field, writer);
			writer.append(" : ").append(value).append("}");
		} catch (IOException e) {
			// nothing to do
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> void toJson(T o, Writer writer) {

		if (o == null) {
			try {
				writer.append("null");
			} catch (IOException e) {

			}
			return;
		}

		Class<?> fieldType = o.getClass();

		if (fieldType.isArray()) {
			writeArray(o, writer);
			return;
		}

		if (Collection.class.isAssignableFrom(fieldType)) {
			writeArray(((Collection<?>) o).toArray(), writer);
			return;
		}

		if (Map.class.isAssignableFrom(fieldType)) {
			writeMap((Map<?, ?>) o, writer);
			return;
		}

		if (fieldType.isEnum()) {
			try {
				writer.append(o.toString());
			} catch (IOException e) {
			}
			return;
		}

		JsonTransformer<?> jt = transformer.get(fieldType);
		if (jt != null) {
			try {

				boolean q = quoted.contains(fieldType);

				if (q) {
					writeQuoted(((JsonTransformer<T>) jt).writer(o), writer);
				} else {
					writer.append(((JsonTransformer<T>) jt).writer(o));
				}

			} catch (IOException e) {
				return;
			}
			return;
		}

		if (o.getClass().isAnnotationPresent(json.parser.JsonObject.class)) {

			try {
				writer.append("{ ");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Field[] fields = o.getClass().getDeclaredFields();
			for (Field f : fields) {
				if (f.isAnnotationPresent(json.parser.JsonField.class)) {
					try {
						JsonField jf = f.getAnnotation(JsonField.class);

						if (jf == null) {
							continue;
						}

						String fieldName = f.getName();

						fieldName = capitalize(fieldName);
						String getter = getter(fieldName, f.getType());

						fieldType = f.getType();

						Method m = o.getClass().getMethod(getter, new Class<?>[] {});

						if (m == null) {
							// Doesn't exist getter method
							continue;
						}

						String jsonName = jf.value();

						writeQuoted(jsonName, writer);
						writer.append(" : ");
						toJson(m.invoke(o), writer);

						int delta = o.getClass().getDeclaringClass() != null ? 2 : 1;
						if (f != fields[fields.length - delta]) {
							writer.append(", ");
						}

					} catch (Throwable ex) {
						ex.printStackTrace();
					}
				}

			}

			try {
				writer.append(" }");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void toJson(String field, Object o, Writer writer) {

		if (o == null || field == null) {
			return;
		}

		try {
			writer.append("{");
			writeQuoted(field, writer);
			writer.append(" : ");
			toJson(o, writer);
			writer.append("}");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String toJson(String field, Object o) {

		if (o == null || field == null) {
			return null;
		}

		StringWriter sw = new StringWriter();
		writeObject(field, toJson(o), sw);

		return sw.toString();
	}

	public static String toJson(Object o) {

		if (o == null) {
			return null;
		}

		StringWriter sw = new StringWriter();

		toJson(o, sw);

		return sw.toString();
	}

	private static String findItem(String field, String json) {

		try {
			JSONObject jo = new JSONObject(json);

			Pattern p = Pattern.compile("([^\\[\\]]*)(\\[.*\\])?");
			Matcher m = p.matcher(field);
			if (m.find() && m.group(2) != null) {
				return jo.getJSONArray(m.group(1)).getString(
						Integer.valueOf(m.group(2).substring(1, m.group(2).length() - 1)));
			}

			return jo.optString(m.group(1), null);
		} catch (JSONException e) {
			return null;
		}
	}

	private static String findField(String field, String json) {
		String[] path = field.split("\\.");

		if (path.length == 1) {
			return findItem(path[0], json);
		}

		String subJson = json;
		for (int i = 0; i < path.length - 1; i++) {
			subJson = findItem(path[i], subJson);
		}

		return findItem(path[path.length - 1], subJson);

	}

	public static <T> T fromJson(String field, Class<T> clazz, String json) {
		return fromJson(clazz, findField(field, json));
	}

	@SuppressWarnings("unchecked")
	public static <T, E> T fromJson(String field, Class<T> clazz, Class<E> itemClass, String json) {
		try {
			JSONObject jsonObject = new JSONObject(json);
			return (T) fromJson(itemClass, clazz, new JSONArray(findField(field, json)));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	
	@SuppressWarnings("unchecked")
	public static <T, E> T fromJson(Class<T> clazz, Class<E> itemClass, String json) {
		try {
			JSONArray jsonArray = new JSONArray(json);
			return (T) fromJson(itemClass, clazz, jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	
	@SuppressWarnings("unchecked")
	public static <T> T fromJson(Class<T> clazz, String json) {

		if (json == null) {
			return null;
		}

		/*
		 * int[], boolean[], long[], double[], String[], enum[]
		 */
		if (clazz.isArray()) {
			try {
				return fromJson(clazz, new JSONArray(json));
			} catch (JSONException e) {

			}

			return null;
		}

		/*
		 * enum class
		 */
		if (clazz.isEnum()) {
			return parseEnum(clazz, json);
		}

		/*
		 * boxing class Integer, Boolean, Double, Long String class Collection
		 * class
		 */
		try {
			if (Collection.class.isAssignableFrom(clazz)) {
				try {
					return fromJson(clazz, new JSONArray(json));
				} catch (JSONException e) {
					e.printStackTrace();
					return null;
				}
			}

			JsonTransformer<?> jt = transformer.get(clazz);
			if (jt != null) {
				return (T) jt.reader(json);
			}

		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
			return null;
		} catch (SecurityException e1) {
			e1.printStackTrace();
			return null;
		}

		/*
		 * At this point an Object is the only chance
		 */
		try {
			return fromJson(clazz, clazz.getDeclaringClass(), new JSONObject(json));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private static <T> T fromJson(Class<T> clazz, JSONArray json) {

		/*
		 * Is a Collection's implementation, for instance, ArrayList,
		 * LinkedList, List, Set
		 */
		if (Collection.class.isAssignableFrom(clazz)) {

			List<Object> items = new ArrayList<Object>();

			for (int i = 0; i < json.length(); i++) {
				try {
					items.add(fromJson(json.get(i).getClass(), json.optString(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			return (T) items;
		}

		/*
		 * Is an array
		 */
		Class<?> itemClass = clazz.getComponentType();
		Object o = Array.newInstance(itemClass, json.length());

		for (int i = 0; i < json.length(); i++) {
			Array.set(o, i, fromJson(itemClass, json.optString(i)));
		}

		return clazz.cast(o);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static <T> T parseEnum(Class<T> clazz, String json) {
		if ("null".equalsIgnoreCase(json) || json == null) {
			return null;
		}

		return (T) Enum.valueOf((Class<Enum>) clazz, json.toUpperCase());
	}

	@SuppressWarnings("unchecked")
	private static <T> List<T> fromJson(Class<?> type, Class<T> clazz, JSONArray json) {

		if (json == null) {
			return null;
		}

		List<T> items = new ArrayList<T>();

		for (int i = 0; i < json.length(); i++) {
			items.add((T) fromJson(type, json.optString(i)));
		}

		return items;
	}

	private static <T> T fromJson(Class<T> clazz, Class<?> parent, JSONObject json) {

		Constructor<?> constructor = clazz.getConstructors()[0];
		Object o = null;
		try {
			if (parent == null) {
				o = constructor.newInstance(new Object[] {});
			} else {
				o = constructor.newInstance(parent.newInstance());
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return null;
		} catch (InstantiationException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}

		if (o == null) {
			return null;
		}

		for (Field field : o.getClass().getDeclaredFields()) {
			if (field.isAnnotationPresent(json.parser.JsonField.class)) {
				try {
					JsonField jf = field.getAnnotation(JsonField.class);

					if (jf == null) {
						continue;
					}

					String fieldName = field.getName();

					fieldName = capitalize(fieldName);
					String setter = setter(fieldName);
					Class<?> fieldType = field.getType();

					Method m = o.getClass().getMethod(setter, new Class<?>[] { fieldType });

					if (m == null) {
						// Doesn't exist setter method
						continue;
					}

					String jsonName = jf.value();

					if (fieldType.isArray()) {
						m.invoke(o, fromJson(fieldType, json.optString(jsonName)));
						continue;
					}

					if (fieldType.isEnum()) {
						m.invoke(o, fromJson(fieldType, json.optString(jsonName)));
						continue;
					}

					if (Collection.class.isAssignableFrom(fieldType)) {
						m.invoke(
								o,
								fromJson((Class<?>) ((ParameterizedType) field.getGenericType())
										.getActualTypeArguments()[0], clazz, json.optJSONArray(jsonName)));
						continue;
					}

					JsonTransformer<?> jt = transformer.get(fieldType);
					if (jt != null) {
						String value = json.optString(jsonName, null);
						m.invoke(o, jt.reader("null".equalsIgnoreCase(value) ? null : value));
						continue;

					}

					// TODO Improve
					/* Is a composite of JSONs */
					Object opt = json.opt(jsonName);

					if (opt == JSONObject.NULL) {
						continue;
					}

					if (opt instanceof JSONObject) {
						m.invoke(o, fromJson(fieldType, fieldType.getDeclaringClass(), json.getJSONObject(jsonName)));
					} else {
						/* Is an Object child but do not belong to JSON */
						m.invoke(o, opt);
					}

				} catch (Throwable ex) {
					ex.printStackTrace();
				}
			}
		}

		return clazz.cast(o);
	}

	private static String capitalize(String name) {
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	private static String setter(String name) {
		return String.format("set%s", name);
	}

	private static String getter(String name, Type type) {

		if (type.toString().equals("boolean")) {
			return "is" + name;
		}

		return "get" + name;
	}

	private static Set<Class<? extends Object>> quoted = new HashSet<Class<? extends Object>>() {
		private static final long serialVersionUID = 5789286756813693633L;

		{
			add(String.class);
			add(Date.class);
			add(Enum.class);
		}
	};

	private static Map<Class<? extends Object>, JsonTransformer<? extends Object>> transformer = new HashMap<Class<?>, JsonParser.JsonTransformer<?>>() {
		private static final long serialVersionUID = 5789286756813693633L;

		{

			// Primitives
			put(int.class, new JsonTransformer<Integer>() {

				@Override
				public Integer reader(String value) {
					return new Integer(value);
				}

				@Override
				public String writer(Integer value) {
					return value.toString();
				}
			});

			put(boolean.class, new JsonTransformer<Boolean>() {

				@Override
				public Boolean reader(String value) {
					return new Boolean(value);
				}

				@Override
				public String writer(Boolean value) {
					return value.toString();
				}
			});

			put(double.class, new JsonTransformer<Double>() {

				@Override
				public Double reader(String value) {
					return new Double(value);
				}

				@Override
				public String writer(Double value) {
					return value.toString();
				}
			});

			put(long.class, new JsonTransformer<Long>() {

				@Override
				public Long reader(String value) {
					return new Long(value);
				}

				@Override
				public String writer(Long value) {
					return value.toString();
				}
			});

			// Boxing / Outboxing
			put(Integer.class, new JsonTransformer<Integer>() {

				@Override
				public Integer reader(String value) {
					return new Integer(value);
				}

				@Override
				public String writer(Integer value) {
					return value.toString();
				}
			});

			put(Boolean.class, new JsonTransformer<Boolean>() {

				@Override
				public Boolean reader(String value) {
					return new Boolean(value);
				}

				@Override
				public String writer(Boolean value) {
					return value.toString();
				}
			});

			put(Double.class, new JsonTransformer<Double>() {

				@Override
				public Double reader(String value) {
					return new Double(value);
				}

				@Override
				public String writer(Double value) {
					return value.toString();
				}
			});

			put(Long.class, new JsonTransformer<Long>() {

				@Override
				public Long reader(String value) {
					return new Long(value);
				}

				@Override
				public String writer(Long value) {
					return value.toString();
				}
			});

			put(String.class, new JsonTransformer<String>() {

				@Override
				public String reader(String value) {
					return value;
				}

				@Override
				public String writer(String value) {
					return value;
				}
			});

			put(Enum.class, new JsonTransformer<Enum<?>>() {

				@Override
				public Enum<?> reader(String value) {
					return null;
				}

				@Override
				public String writer(Enum<?> value) {
					return value.toString();
				}
			});

		}
	};

	public static void addTransformer(Class<? extends Object> clazz, JsonTransformer<? extends Object> jsonTransformer) {
		transformer.put(clazz, jsonTransformer);
	}

	public interface JsonTransformer<T extends Object> {
		public T reader(String value);

		public String writer(T value);
	}
}
